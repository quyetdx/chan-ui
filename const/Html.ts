export class Html {
    static basePopup: string =
        '<div id="base-popup" class="base-popup">'
        + '<div id="popup-title"></div>'
        + '<button id="popup-close" type="button"></button>'
        + '<div id="popup-content"></div>'
        + '</div>';
    static popupTool: string = '<div class="popup-tool">'
        + '<div id="horizontalLine"></div>'
        + '<button class="btn btn-default btn-sm BtnBase" id="popup-ok">Chấp nhận</button>'
        + '</div>'
    static popupInsideBg: string = '<div id="popupInsideBg"></div>'
}