import Theme = GOWN.Theme;
import ScrollBar = GOWN.ScrollBar;
import ScrollThumb = GOWN.ScrollThumb;
import Rectangle = PIXI.Rectangle;
export class SDTheme extends Theme {
    constructor(jsonPath: any, global?: any) {
        super(global)

        if (jsonPath) this.addImage(jsonPath);
        this.once(Theme.COMPLETE, this.onloadComplete);
    }

    static HORIZONTAL_SCROLL_BAR_TRACK_SCALE_9_GRID: Rectangle = new Rectangle(1, 2, 2, 11);
    static VERTICAL_SCROLL_BAR_TRACK_SCALE_9_GRID: Rectangle = new Rectangle(2, 2, 4, 231);
    static VERTICAL_SCROLL_BAR_Thumbb_SCALE_9_GRID: Rectangle = new Rectangle(3, 6, 2, 8);

    onloadComplete = (): void => {
        // ScrollBar
        // this.setSkin(ScrollBar.SKIN_NAME, "horizontal_track", this.getScaleContainer("tray", SDTheme.HORIZONTAL_SCROLL_BAR_TRACK_SCALE_9_GRID));
        this.setSkin(ScrollBar.SKIN_NAME, "vertical_track", this.getScaleContainer("tray", SDTheme.VERTICAL_SCROLL_BAR_TRACK_SCALE_9_GRID));
        // this.setSkin(ScrollBar.SKIN_NAME, 'horizontal_up', this.getImage('slider_thumb_up'));

        // this.setSkin(ScrollBar.SKIN_NAME, "horizontal_progress", function () {
        //         return new GOWN.Rect(0xff0000, 1.0, 10, 10);
        //     }
        // );
        this.setSkin(ScrollBar.SKIN_NAME, "vertical_progress", function () {
                return new GOWN.Rect(0xff0000, 1.0, 10, 10);
            }
        );

        // ScrollThumb
        // this.setSkin(ScrollThumb.SKIN_NAME, "horizontal_up", this.getImage("slider_thumb_up"));
        // this.setSkin(ScrollThumb.SKIN_NAME, "horizontal_down", this.getImage("slider_thumb_up"));
        // this.setSkin(ScrollThumb.SKIN_NAME, "horizontal_hover", this.getImage("slider_thumb_up"));
        // this.setSkin(ScrollThumb.SKIN_NAME, "horizontal_thumb", this.getImage("slider_thumb_up"));

        this.setSkin(ScrollThumb.SKIN_NAME, "vertical_up" , this.getScaleContainer("thumb", SDTheme.VERTICAL_SCROLL_BAR_Thumbb_SCALE_9_GRID));
        this.setSkin(ScrollThumb.SKIN_NAME, "vertical_down", this.getScaleContainer("thumb", SDTheme.VERTICAL_SCROLL_BAR_Thumbb_SCALE_9_GRID));
        this.setSkin(ScrollThumb.SKIN_NAME, "vertical_hover", this.getScaleContainer("thumb", SDTheme.VERTICAL_SCROLL_BAR_Thumbb_SCALE_9_GRID));
        this.setSkin(ScrollThumb.SKIN_NAME, "vertical_thumb", this.getScaleContainer("thumb", SDTheme.VERTICAL_SCROLL_BAR_Thumbb_SCALE_9_GRID));
    }
}