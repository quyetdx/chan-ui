This project is used to create view for use in project ts-chan.
Create view from this project is faster than in project ts-chan & works completely in your local machine

To use this project:
1. Clone repo
2. Run npm i to install all dependencies
3. Open your terminal, then run webpack-dev-server
4. Open browser & go to localhost:8080/webpack-dev-server to see the result