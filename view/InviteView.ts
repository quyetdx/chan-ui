import {BasePopup} from "./BasePopup";
import {SDScrollPane} from "../component/SDScrollPane";
import {PlayerInfoComponent} from "../component/PlayerInfoComponent";
import view = require("../core/view");
import Text = PIXI.Text;
import Sprite = PIXI.Sprite;
export class InviteView extends BasePopup {
    @view({type: SDScrollPane, x: 18, y: 37, w: 500, h: 340})
    sp: SDScrollPane;

    constructor() {
        super('ABCDEFGHIKLMNOPQRSTUVWXY', 564, 410)

        let items: Array<PlayerInfoComponent> = []
        for (let i = 0; i < 10; i++) {
            let item = new PlayerInfoComponent('invite', 'invited', 'invite_dis')
            items.push(item)
        }

        this.sp.source = items
    }
}