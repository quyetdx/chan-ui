import {assign, MultiStyleText, TextData, TextStyleSet} from '../MultiStyleText'
import {sortBy} from 'lodash'
import ITextStyleStyle = PIXI.ITextStyleStyle
import {App} from '../../const/App'
import {Point2D} from './PlayGif'

let jEmoticon = JSON.parse('[{"title":"vui vẻ","keys":[":)",":-)"],"id":1},{"title":"nhăn mặt","keys":[":(",":-("],"id":2},{"title":"nháy mắt","keys":[";)",";-)"],"id":3},{"title":"cười nhăn răng","keys":[":D",":-D"],"id":4},{"title":"đá lông nheo","keys":[";;)"],"id":5},{"title":"ôm một cái","keys":[">:D<"],"id":6},{"title":"bối rối","keys":[":-/",":-\\\\"],"id":7},{"title":"yêu thế","keys":[":x",":X",":-x",":-X"],"id":8},{"title":"thẹn thùng","keys":[":\\">",":$"],"id":9},{"title":"lè lưỡi","keys":[":P",":p",":-p",":-P"],"id":10},{"title":"chụt chụt","keys":[":-*",":*"],"id":11},{"title":"tan nát cõi lòng","keys":["=(("],"id":12},{"title":"ngạc nhiên","keys":[":-O",":-o",":o",":O"],"id":13},{"title":"giận dữ","keys":["X(","x(","x-(","X-(",":@"],"id":14},{"title":"vênh mặt","keys":[":>",":->"],"id":15},{"title":"ngầu","keys":["B-)","B)"],"id":16},{"title":"lo lắng","keys":[":-S",":-s",":s",":-S"],"id":17},{"title":"phù!","keys":["#:-S","#:-s"],"id":18},{"title":"quỷ sứ","keys":[">:)"],"id":19},{"title":"khóc rồi nè","keys":[":((",":-(("],"id":20},{"title":"cười ngoác miệng","keys":[":))",":-))"],"id":21},{"title":"chịu","keys":[":|",":-|"],"id":22},{"title":"nhíu mày","keys":["/:)","/:-)"],"id":23},{"title":"cười lăn lộn","keys":["=))"],"id":24},{"title":"thiên thần","keys":["O:-)","o:-)","O:)","o:)"],"id":25},{"title":"mọt sách","keys":[":-B",":B"],"id":26},{"title":"đủ rồi","keys":["=;"],"id":27},{"title":"buồn ngủ","keys":["I-)"],"id":28},{"title":"tròn mắt","keys":["8-|"],"id":29},{"title":"yếu mà ra gió","keys":["l-)"],"id":30},{"title":"không chịu nổi","keys":[":-&"],"id":31},{"title":"im lặng","keys":[":-$"],"id":32},{"title":"không thèm nói","keys":["[-("],"id":33},{"title":"làm mặt hề","keys":[":O)",":o)"],"id":34},{"title":"ặc ặc","keys":["8-}"],"id":35},{"title":"toe toe","keys":["<:-P","<:-p"],"id":36},{"title":"mệt mỏi","keys":["(:|"],"id":37},{"title":"thèm nhỏ dãi","keys":["=P~","=p~"],"id":38},{"title":"suy nghĩ","keys":[":-?"],"id":39},{"title":"trời ơi!","keys":["#-o","#-O"],"id":40},{"title":"vỗ tay","keys":["=D>"],"id":41},{"title":"cắn móng tay","keys":[":-SS",":-ss"],"id":42},{"title":"bị thôi miên","keys":["@-)"],"id":43},{"title":"nói dối","keys":[":^o",":^O"],"id":44},{"title":"đang đợi nè","keys":[":-w",":-W"],"id":45},{"title":"thở dài","keys":[":-<"],"id":46},{"title":"phbbbt","keys":[">:P",">:p"],"id":47},{"title":"đang bận điện thoại","keys":[":)]"],"id":100},{"title":"lợn con","keys":[":@)"],"id":101},{"title":"hết cách","keys":["~x(","~X("],"id":102},{"title":"bái bai","keys":[":-h"],"id":103},{"title":"hết giờ","keys":[":-t"],"id":104},{"title":"mơ giữa ban ngày","keys":["8->"],"id":105},{"title":"sợ quá đi thôi","keys":["X_X","x_x"],"id":109},{"title":"nhanh lên!","keys":[":!!"],"id":110},{"title":"yeah!","keys":["m/"],"id":111},{"title":"không đồng ý","keys":[":-q"],"id":112},{"title":"đồng ý hai tay","keys":[":-bd"],"id":113},{"title":"không phải tôi!","keys":["^#(^"],"id":114}]')
export type StringArray = string[]
let keysArray: StringArray[] = []
jEmoticon.forEach((emoticon: Emoticon) => {
    keysArray.push(emoticon.keys)
})

export interface Emoticon {
    title: string,
    keys: StringArray,
    id: number
}

interface EmoticonLocation {
    title: string,
    key: string,
    id: number,
    index: number // index in string
}

/**
 * @desc: create a text which can play gif
 */
export class TextGif extends MultiStyleText {
    getTextDataPerLine(lines: string[]) {
        let outputTextData: TextData[][] = [];
        let styleStack = assign({}, this.textStyles["default"]);

        // determine the group of word for each line
        for (let i = 0; i < lines.length; i++) {
            let lineTextData: TextData[] = [];
            lineTextData.push(this.createTextData(lines[i], styleStack))
            outputTextData.push(lineTextData);
        }

        return outputTextData;
    }
    stringEmoticonData: Array<string | EmoticonLocation>
    header: string
    content: string
    /**
     *
     * @param {string} text
     * @param {TextStyleSet} styles
     */
    constructor(text?: string, styles?: TextStyleSet) {
        super(text, styles);
        // split message header & content, locate string & emoticon in content
        let iSeparatortext = text.indexOf(': ') + 2
        this.header = text.substr(0, iSeparatortext)
        this.content = text.substr(iSeparatortext)
        this.stringEmoticonData = this.locateStringEmoticon(this.content)
    }

    /**
     * @desc locate index of emoticons in chat msg with their data
     * @param {string} msg
     * @return {EmoticonLocation[]}
     */
    locateEmoticons(msg: string): EmoticonLocation[] {
        let emoticonLocations: EmoticonLocation[] = []
        keysArray.forEach((keys: string[], iKey: number) => {
            keys.forEach((key) => {
                let i = msg.indexOf(key)
                while (i > -1) {
                    // emoticon & emoticon keys have same index in their array
                    let emoticon: Emoticon = jEmoticon[iKey]
                    let emoticonLocation: EmoticonLocation = {
                        title: emoticon.title, key: key, id: emoticon.id, index: i
                    }
                    emoticonLocations.push(emoticonLocation)
                    i = msg.indexOf(key, i + 1)
                }
            })
        })
        // If there are emoticons which overlap each other characters, keep only the longest key len emoticon
        for (let i = 0; i < emoticonLocations.length; i++) {
            let iLongestKeyLen = i
            let lastEmoticonLocationIndex = emoticonLocations[i].index + emoticonLocations[i].key.length - (emoticonLocations[i].key.length > 0 ? 1 : 0)
            // Examine other emoticonLocation
            let j = i + 1
            while (j < emoticonLocations.length) {
                let lastEmoticonLocation2Index = emoticonLocations[j].index + emoticonLocations[j].key.length - (emoticonLocations[j].key.length > 0 ? 1 : 0)
                // if 1st index or last index of one is between 1st & last index of other, these 2 are overlap each other characters
                if ((emoticonLocations[j].index >= emoticonLocations[i].index && emoticonLocations[j].index <= lastEmoticonLocationIndex) || (lastEmoticonLocation2Index >= emoticonLocations[i].index && lastEmoticonLocation2Index <= lastEmoticonLocationIndex)) {
                    // Note: must remove the shorter key len emoticonLocation to prevent examining it again
                    let iNeedRemove: number
                    if (emoticonLocations[j].key.length > emoticonLocations[iLongestKeyLen].key.length) {
                        iNeedRemove = iLongestKeyLen
                        iLongestKeyLen = j - 1
                    } else {
                        iNeedRemove = j
                    }
                    emoticonLocations.splice(iNeedRemove, 1)
                } else {
                    j++
                }
            }
            // if found longest key len emoticonLocation in another place, swaps it with 1st emoticonLocation to prevent examining it again
            if (iLongestKeyLen > i) {
                let temp = emoticonLocations[i]
                emoticonLocations[i] = emoticonLocations[iLongestKeyLen]
                emoticonLocations[iLongestKeyLen] = temp
            }
        }
        return emoticonLocations
    }

    /**
     * @desc locate index of string & emoticons in chat msg with their data
     * @param {string} msg
     * @return {Array<string | EmoticonLocation>}
     */
    locateStringEmoticon(msg: string): Array<string | EmoticonLocation> {
        let stringEmoticonData: Array<string | EmoticonLocation>
        stringEmoticonData = this.locateEmoticons(msg)
        stringEmoticonData = sortBy(stringEmoticonData, 'index')
        // Insert string data in order they appear
        let iFirstChar = 0
        for (let i = 0; i < stringEmoticonData.length; i++) {
            let emoticon = stringEmoticonData[i] as EmoticonLocation
            if (emoticon.index > iFirstChar) {
                let str = msg.substring(iFirstChar, emoticon.index)
                stringEmoticonData.splice(i, 0, str)
                i++
            }
            iFirstChar += emoticon.index + emoticon.key.length
        }
        // Check if there is string after last emoticon
        if (iFirstChar < msg.length) {
            let str = msg.substring(iFirstChar, msg.length)
            stringEmoticonData.push(str)
        }
        return stringEmoticonData
    }

    public updateText(): void {
        if (!this.dirty) {
            return;
        }
        // set drawing tool up
        this.context.scale(this.resolution, this.resolution);
        this.context.textBaseline = 'alphabetic';
        this.context.lineJoin = 'round';
        this.texture.baseTexture.resolution = this.resolution;
        this.canvas.width = (this._style.wordWrapWidth > this.parent.width ? this._style.wordWrapWidth : this.parent.width)
        
        let linePositionX: number = 0
        let linePositionY: number = 0
        let textStyles = this.textStyles;
        let outputTextEmoticonData: (TextWithSizeData | EmoticonData)[] = []
        // read data contains what will be drawn
        // Todo: rewrite calculate outputTextData for subsequent outputText
        
        this.stringEmoticonData.forEach((value: string | EmoticonLocation) => {
            if (typeof value == 'string') {
                let outputText = value;
                // split text into lines
                let lines = outputText.split(/(?:\r\n|\r|\n)/);
                // get the text data with specific styles
                let outputTextData = this.getTextDataPerLine(lines);
                // calculate text width and height
                let lineWidths: number[] = [];
                let lineHeights: number[] = [];
                let maxLineWidth = 0;
                
                for (let i = 0; i < outputTextData.length; i++) {
                    let lineWidth = 0;
                    let lineHeight = 0;
                    for (let j = 0; j < outputTextData[i].length; j++) {
                        // save the font properties
                        outputTextData[i][j].fontProperties = PIXI.Text.calculateFontProperties(PIXI.Text.getFontStyle(this._style));
                        // save the height
                        outputTextData[i][j].height = outputTextData[i][j].fontProperties.fontSize + outputTextData[i][j].style.strokeThickness;
                        lineHeight = Math.max(lineHeight, outputTextData[i][j].height);
                        // Workaround for specific font style make PIXI calculate wrong font properties
                        this.context.font = PIXI.Text.getFontStyle(outputTextData[i][j].style);
                        // save the width
                        if (this._style.wordWrap === true) {
                            let textWidth = this.context.measureText(outputTextData[i][j].text).width;
                            if (textWidth + lineWidth > this._style.wordWrapWidth) {
                                const charNum = Math.floor(this._style.wordWrapWidth / textWidth * outputTextData[i][j].text.length);
                                // get more chars to fill space to have width close to word wrap width
                                let charNeed = Math.floor((this._style.wordWrapWidth - lineWidth) / textWidth * outputTextData[i][j].text.length);
                                let text = outputTextData[i][j].text.substr(0, charNeed);
                                // if the calculated charNeed wrong, update it
                                while (this.context.measureText(text).width + lineWidth > this._style.wordWrapWidth) {
                                    charNeed--;
                                    text = text.substr(0, charNeed);
                                }
                                // process breakWords if there is char left after wrapping
                                if (charNeed < outputTextData[i][j].text.length) {
                                    if (this._style.breakWords === true) {
                                        let i = text.lastIndexOf(' ');
                                        if (i !== -1) {
                                            text = text.substr(0, i);
                                            charNeed = i + 1; // throw away the space
                                        } else {
                                            // if line already had character & we cut @ the middle of a word, don't take any chars
                                            if (j > 0) {
                                                charNeed = 0;
                                                text = text.substr(0, charNeed);
                                            }
                                        }
                                    }
                                }
                                // put those chars to outputTextData
                                let leftText = outputTextData[i][j].text.substr(charNeed);
                                outputTextData[i][j].text = text;
                                outputTextData[i][j].width = this.context.measureText(outputTextData[i][j].text).width;
                                lineWidth += outputTextData[i][j].width;
                                // put each piece of cut text to outputTextData array
                                let k = j;
                                let oldI = i;
                                while (leftText.length > 0) {
                                    lineWidths[i] = lineWidth;
                                    lineHeights[i] = lineHeight;
                                    if (lineWidth > maxLineWidth) {
                                        maxLineWidth = lineWidth;
                                    }
                                    let copy = <TextData>JSON.parse(JSON.stringify(outputTextData[i][k]));
                                    let num = charNum;
                                    copy.text = leftText.substr(0, num);
                                    while (this.context.measureText(copy.text).width > this._style.wordWrapWidth) {
                                        num--;
                                        copy.text = copy.text.substr(0, num);
                                    }
                                    if (num < leftText.length) {
                                        if (this._style.breakWords === true) {
                                            let idx = copy.text.lastIndexOf(' ');
                                            if (idx !== -1) {
                                                num = idx;
                                            }
                                            copy.text = copy.text.substr(0, num);
                                            num++;
                                        }
                                    }
                                    lineWidth = copy.width = this.context.measureText(copy.text).width;
                                    let array = [];
                                    array.push(copy);
                                    i++;
                                    outputTextData.splice(i, 0, array);
                                    lineWidths[i] = lineWidth;
                                    lineHeights[i] = lineHeight;
                                    if (lineWidth > maxLineWidth) {
                                        maxLineWidth = copy.width;
                                    }
                                    k = 0;
                                    leftText = leftText.substr(num);
                                }
                                j++;
                                if (j < outputTextData[oldI].length) {
                                    outputTextData[i].splice(1, 0, ...outputTextData[oldI].splice(j));
                                    j = 0;
                                } else {
                                    lineWidths.pop();
                                    lineHeights.pop();
                                }
                            } else {
                                outputTextData[i][j].width = textWidth;
                                lineWidth += outputTextData[i][j].width;
                            }
                        } else {
                            outputTextData[i][j].width = this.context.measureText(outputTextData[i][j].text).width;
                            lineWidth += outputTextData[i][j].width;
                        }
                    }
                    lineWidths[i] = lineWidth;
                    lineHeights[i] = lineHeight;
                    maxLineWidth = Math.max(maxLineWidth, lineWidth);
                }
                // transform styles in array
                let stylesArray = Object.keys(textStyles).map((key) => textStyles[key]);
                let maxStrokeThickness = stylesArray.reduce((prev, curr) => Math.max(prev, curr.strokeThickness || 0), 0);

                // calculate position to display
                let positions: Point2D[][] = []
                for (let i = 0; i < outputTextData.length; i++) {
                    let line = outputTextData[i];
                    let linePosition = positions[i]

                    for (let j = 0; j < line.length; j++) {
                        let textStyle = line[j].style;
                        let text = line[j].text;
                        let fontProperties = line[j].fontProperties;

                        this.context.font = PIXI.Text.getFontStyle(textStyle);
                        this.context.strokeStyle = <string>textStyle.stroke;
                        this.context.lineWidth = textStyle.strokeThickness;

                        linePositionX += maxStrokeThickness / 2;

                        if (this._style.align === "right") {
                            linePositionX += maxLineWidth - lineWidths[i];
                        }
                        else if (this._style.align === "center" && (linePositionX === 0 || maxStrokeThickness !== 0)) {
                            linePositionX += (maxLineWidth - lineWidths[i]) / 2;
                        }

                        if (textStyle.valign === "bottom") {
                            linePositionY += lineHeights[i] - line[j].height -
                                (maxStrokeThickness - textStyle.strokeThickness) / 2;
                        } else if (textStyle.valign === "middle") {
                            linePositionY += (lineHeights[i] - line[j].height) / 2 -
                                (maxStrokeThickness - textStyle.strokeThickness) / 2;
                        }

                        // draw shadow
                        if (textStyle.dropShadow) {
                            this.context.fillStyle = <string>textStyle.dropShadowColor;

                            let xShadowOffset = Math.sin(textStyle.dropShadowAngle) * textStyle.dropShadowDistance;
                            let yShadowOffset = Math.cos(textStyle.dropShadowAngle) * textStyle.dropShadowDistance;

                            if (textStyle.fill) {
                                this.context.fillText(text, linePositionX + xShadowOffset, linePositionY + yShadowOffset);
                            }
                        }

                        // set canvas text styles
                        this.context.fillStyle = <string>textStyle.fill;

                        // draw lines
                        if (textStyle.stroke && textStyle.strokeThickness) {
                            this.context.strokeText(text, linePositionX, linePositionY);
                        }

                        if (textStyle.fill) {
                            this.context.fillText(text, linePositionX, linePositionY);
                        }

                        linePosition.push({
                            x: linePositionX,
                            y: linePositionY + maxStrokeThickness / 2 + fontProperties.ascent 
                        })
                        // set Position X to the line width
                        // remove the strokeThickness otherwise the text will be too far from the previous group
                        linePositionX += line[j].width;
                        linePositionX -= maxStrokeThickness / 2
                        linePositionY += lineHeights[i];
                    }
                }

                // save data
                outputTextEmoticonData.push({
                    kind: 'TextWithSizeData',
                    outputTextData: outputTextData,
                    lineWidths: lineWidths,
                    lineHeights: lineHeights,
                    maxLineWidth: maxLineWidth,
                    maxStrokeThickness: maxStrokeThickness
                })
            } else if (typeof value == 'object') {
                value = value as EmoticonLocation
                let emoticonData: EmoticonData = {
                    kind: 'EmoticonData',
                    data: App.GifData[value.id]
                }
                outputTextEmoticonData.push(emoticonData)
            }
        })

        // Todo: 
        this.updateTexture();
    }
}

interface EmoticonData {
    kind: 'EmoticonData'
    data: string
}
// includes output text data & size data to draw it on canvas
interface TextWithSizeData {
    kind: 'TextWithSizeData'
    outputTextData: TextData[][],
    lineWidths: number[],
    lineHeights: number[],
    maxStrokeThickness: number,
    maxLineWidth: number
}