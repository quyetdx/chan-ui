import {TextGif} from './TextGif'
import TextStyle = PIXI.TextStyle

export class ChatText {
    sprite: TextGif
    constructor(msg: string, color = 'white', isMod = false) {
        this.sprite = new TextGif(msg, {
            default: new TextStyle({
                fill: color
            })
            , name: {
                fill: isMod ? 'yellow' : 'blue'
            }
        })
    }
}