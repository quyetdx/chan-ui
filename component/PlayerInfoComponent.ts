import CheckBox = GOWN.CheckBox;
import {Img} from "../const/Img";
import Theme = GOWN.Theme;
import Sprite = PIXI.Sprite;
import Texture = PIXI.Texture;
import TextureCache = PIXI.utils.TextureCache;
import {ThemeConfig} from "../config/ThemeConfig";
export class PlayerInfoComponent extends GOWN.CheckBox {
    private static themes: Theme = new Theme(Img.JThemePath);
    
    private iconDis: Sprite = new Sprite();
    private iconDone: Sprite = new Sprite();
    private iconUp: Sprite = new Sprite();
    private _enable: boolean;
    
    constructor(iconUp: string, iconDone: string, iconDis: string, preselected = false) {
        super(preselected, PlayerInfoComponent.themes, CheckBox.SKIN_NAME);
        
        this.width = 243;
        this.height = 83;
        this.buttonMode = true;
        this.initIcon(iconUp, iconDone, iconDis);
        this.enable = true
        PlayerInfoComponent.initTheme();
    }

    static initTheme = () => {
        PlayerInfoComponent.themes.setSkin(CheckBox.SKIN_NAME, CheckBox.UP, ThemeConfig.CORE.getImage('bg_up'));
        PlayerInfoComponent.themes.setSkin(CheckBox.SKIN_NAME, CheckBox.DOWN, ThemeConfig.CORE.getImage('bg_up'));
        PlayerInfoComponent.themes.setSkin(CheckBox.SKIN_NAME, CheckBox.HOVER, ThemeConfig.CORE.getImage('bg_hover'));
        PlayerInfoComponent.themes.setSkin(CheckBox.SKIN_NAME, CheckBox.DISABLE, ThemeConfig.CORE.getImage('bg_dis'));
        PlayerInfoComponent.themes.setSkin(CheckBox.SKIN_NAME, CheckBox.SELECTED_UP, ThemeConfig.CORE.getImage('bg_selected'));
        PlayerInfoComponent.themes.setSkin(CheckBox.SKIN_NAME, CheckBox.SELECTED_DOWN, ThemeConfig.CORE.getImage('bg_selected'));
        PlayerInfoComponent.themes.setSkin(CheckBox.SKIN_NAME, CheckBox.SELECTED_HOVER, ThemeConfig.CORE.getImage('bg_selected'));
    };

    get enable(): boolean {
        return this._enable;
    }

    set enable(value: boolean) {
        this.interactive = this._enable = value;
        
        this.iconDone.visible = false
        
        this.iconUp.visible = this.enable
        this.iconDis.visible = !this.iconUp.visible
    }

    initIcon = (iconUp: string, iconDone: string, iconDis: string) => {
        this.iconUp.texture = Texture.from(TextureCache[iconUp]);
        this.iconDis.texture = Texture.from(TextureCache[iconDis]);
        this.iconDone.texture = Texture.from(TextureCache[iconDone]);
        this.iconUp.x = this.iconDis.x = this.iconDone.x = 9;
        this.iconUp.y = this.iconDis.y = this.iconDone.y = 8;
        this.addChild(this.iconUp);
        this.addChild(this.iconDis);
        this.addChild(this.iconDone);
    };
}